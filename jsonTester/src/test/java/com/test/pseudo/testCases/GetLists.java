package com.test.pseudo.testCases;

public class GetLists {
	
	/*
	 * 1. For status code success 200 check 
	 * when()
	 * issue get api call with valid api_key parameter, and movie id 295693
	 * then()
	 * assert json response
	 * assert status code of 200
	 * assert json body for correctness for movie id 295693
	 * 
	 * 2. For status code 401 check
	 * when()
	 * issue get api call with invalid api_key param, and valid movie id
	 * then()
	 * assert json response
	 * assert status code of 401 (forbidden)
	 * assert status message invalid api key
	 * 
	 * 3. For status code 404 check
	 * when()
	 * issue get api call with invalid movie id, and valid api_key param
	 * then()
	 * assert json response
	 * assert status code of 404 (Not found)
	 * assert status message can't find the content
	 * 
	 * 4. For status code 422 check
	 * when()
	 * issue get api call with page number > 1000, all other valid params
	 * then()
	 * assert json response
	 * assert status code of 422
	 * assert error content information
	 */
}
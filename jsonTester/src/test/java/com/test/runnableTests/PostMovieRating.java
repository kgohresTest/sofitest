package com.test.runnableTests;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import com.test.constants.Constants;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class PostMovieRating {
	/**
	 * Test case for testing using a post json call for the post moving rating api call
	 * 
	 * Test steps:
	 * 1. Setup guest_session_id by calling get create guest session api, and saving
	 * unique id that is generated.
	 * 
	 * 2. Using the rating value of 8.5 in a json attempt to post a moving rating using
	 * a correct api key, a valid movie ID, and a valid guest session id (generated from step 1).
	 * 
	 * 3. Verify that we should get the following returned:
	 * a. A status code of 201 (success)
	 * b. The content should be in JSON format
	 * c. The status message should read success
	 * 
	 * 4. Using the same json value to post the rating, try an invalid session id with
	 * correct API_KEY, and movie ID
	 * 
	 * 5. Verify that we should get the following returned:
	 * a. A status code of 401 (forbidden)
	 * b. The content should be in JSON format
	 * c. The status message should read, Authentication failed: You do not have permissions to access the service.
	 * 
	 * 6. Using the same json value to post the rating again, this time try to use an invalid API_KEY,
	 * with a correct guest_session_id, and movie ID.
	 * 
	 * 7. Verify that we should get the following returned:
	 * a. A status code of 401 (forbidden)
	 * b. The content should be in JSON format
	 * c. The status message should read, Invalid API_KEY: You must enter a valid key.
	 * 
	 * 8. Finally try to post the movie rating again, this time try with an invalid movie ID,
	 * but valid API_KEY, and guest_session_id.
	 * 
	 * 9. Verify that we should get the following returned:
	 * a. A status code of 404 (Not found)
	 * b. The content should be in JSON format
	 * c. The status message should read, The resource you requested could not be found.
	 */
	@Test
	public void testPostMovieRatingOutcomes()
	{
		//Step 1
		Response response = 
				when().
					get(getURL(),Constants.API_KEY).
				then().
					contentType(ContentType.JSON).
					statusCode(200).
				extract().
					response();
		String guest_session_id = response.path("guest_session_id");
		
		//Steps 2-3
		String myJson = "{\"value\": 8.5}";
		given().
		header("Content-Type", "application/json").
		body(myJson).
		when().
			post(postURL(),Constants.ID,Constants.API_KEY,guest_session_id).
		then().
			contentType(ContentType.JSON).
			statusCode(201).
			body("status_message", equalTo("Success."));
		
		//Steps 4-5
		given().
		header("Content-Type", "application/json").
		body(myJson).
		when().
			post(postURL(),Constants.ID,Constants.API_KEY,"AnInvalidSessionId").
		then().
			contentType(ContentType.JSON).
			statusCode(401).
			body("status_message", equalTo("Authentication failed: You do not have permissions to access the service."));
		
		//Steps 6-7
		given().
		header("Content-Type", "application/json").
		body(myJson).
		when().
			post(postURL(),Constants.ID,"AnInvalidAPIKey",guest_session_id).
		then().
			contentType(ContentType.JSON).
			statusCode(401).
			body("status_message", equalTo("Invalid API key: You must be granted a valid key."));
		
		//Steps 8-9
		given().
		header("Content-Type", "application/json").
		body(myJson).
		when().
			post(postURL(),139393939,Constants.API_KEY,guest_session_id).
		then().
			contentType(ContentType.JSON).
			statusCode(404).
			body("status_message", equalTo("The resource you requested could not be found."));
		
		
	}
	//returns a built URL to use during a get api call
	public String getURL(){
		Constants constantVars = new Constants();
		String url = constantVars.urlBuilder("?api_key={API_KEY}", Constants.API_PARAM2);
		//a check to make sure we didn't get an error when building the url
		assertNotNull(url);
		return url;
	}
	//returns a built URL to use during a post api call
	public String postURL(){
		Constants constantVars = new Constants();
		String url = constantVars.urlBuilder("{id}/rating?api_key={API_KEY}&guest_session_id={GUEST_SESSION_ID}", Constants.API_PARAM);
		//a check to make sure we didn't get an error when building the url
		assertNotNull(url);
		return url;
	}
}
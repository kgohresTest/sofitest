package com.test.runnableTests;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import com.test.constants.Constants;

import io.restassured.http.ContentType;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class GetMovieDetails {
	/**
	 * Test case for testing using a get json call for the get movie details api call
	 * 
	 * Test steps:
	 * 0. This test case is broken into 3 different junit runs marked by the @Test:
	 * testSuccess (tests when a success json api call is made)
	 * test401Run (tests when an invalid api key is provided)
	 * test404Run (tests when an invalid movie ID is provided)
	 * 
	 * For testSucess():
	 * 1. Sets up the get call url and passes in a valid get api call to the movie details api
	 * 2. Verifies the following:
	 * a. Response message should be of JSON type
	 * b. Status code returned was 200 (success)
	 * c. Checks the return json body for correctness for the movie ID that was passes
	 * **Note, I did leave a few of the json schema checks out for the values that could change by
	 * the time you go to test this, like movie rating, gross revenue, etc..
	 * 
	 * For test401Run():
	 * 1. Sets up the get call url and passes in an invalid get api call (sends in an invalid api key)
	 * 2. Verifies the following:
	 * a. Response message should be of JSON type
	 * b. Status code should be 401 (forbidden)
	 * c. Status message in returning json should read, Invalid API key: You must be granted a valid key.
	 * 
	 * For test404Run():
	 * 1. Sets up the get call url and passes in an invalid get api call (this time with invalid movie ID)
	 * 2. Verifies the following:
	 * a. Response message should be of JSON type
	 * b. Status code should be 404 (Not found)
	 * c. Status message in returning json should read, "The resource you requested could not be found."
	 */
	
	// a successful run, and checking the responding json for correctness.
	@Test
	public void testSuccess()
	{		
		when().
		get(getURL(),Constants.ID,Constants.API_KEY).
		then().
			contentType(ContentType.JSON).
			statusCode(200).
			body("adult", equalTo(false),
			"backdrop_path", equalTo("/bTFeSwh07oX99ofpDI4O2WkiFJ.jpg"),
			"belongs_to_collection", equalTo(null),
			"budget", equalTo(125000000),
			"genres.id", hasItems(16,35,10751),
			"genres.name", hasItems("Animation","Comedy","Family"),
			"homepage", equalTo("http://www.dreamworks.com/thebossbaby/"),
			"id", equalTo(Constants.ID),
			"imdb_id", equalTo("tt3874544"),
			"original_language", equalTo("en"),
			"original_title", equalTo("The Boss Baby"),
			"overview", equalTo("A story about how a new baby's arrival impacts a family, told from the point of view of a delightfully unreliable narrator, a wildly imaginative 7 year old named Tim."),
			"poster_path", equalTo("/unPB1iyEeTBcKiLg8W083rlViFH.jpg"),
			"production_companies.name", hasItems("Twentieth Century Fox Film Corporation","DreamWorks Animation"),
			"production_companies.id", hasItems(306,521),
			"production_countries.iso_3166_1", hasItems("US"),
			"production_countries.name", hasItems("United States of America"),
			"release_date", equalTo("2017-03-23"),
			"runtime", equalTo(97),
			"spoken_languages.iso_639_1", hasItems("en"),
			"spoken_languages.name", hasItems("English"),
			"status", equalTo("Released"),
			"tagline", equalTo("Born leader"),
			"title", equalTo("The Boss Baby"),
			"video", equalTo(false)
			);
	}
	//test using invalid movie ID
	@Test
	public void test404Run()
	{
		when().
		get(getURL(),139393939,Constants.API_KEY).
		then().
			contentType(ContentType.JSON).
			statusCode(404).
			body("status_message", equalTo("The resource you requested could not be found."));
	}
	//test using invalid api key
	@Test
	public void test401Run()
	{
		when().
		get(getURL(),Constants.ID,"AnInvalidAPIKey").
		then().
			contentType(ContentType.JSON).
			statusCode(401).
			body("status_message", equalTo("Invalid API key: You must be granted a valid key."));
	}
	//setup for the get api URL
	public String getURL(){
		Constants constantVars = new Constants();
		String url = constantVars.urlBuilder("{id}?api_key={API_KEY}",Constants.API_PARAM);
		//a check to make sure we didn't get an error when building the url
		assertNotNull(url);
		return url;
	}
}
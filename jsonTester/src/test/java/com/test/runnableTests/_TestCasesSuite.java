package com.test.runnableTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite for runnable test cases
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	
	GetMovieDetails.class,
	PostMovieRating.class
	})

public class _TestCasesSuite {
	static {
		System.out.println("In TestCasesSuite static init.");
	}
}
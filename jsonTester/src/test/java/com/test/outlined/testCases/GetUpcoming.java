package com.test.outlined.testCases;

public class GetUpcoming{
	/*
	 * Test steps for 200 success:
	 * 1. Setup get api URL with valid parameters
	 * 2. Execute the call
	 * 3. Check return response for the following:
	 * a. Status code of 200
	 * b. content type should be json
	 * c. check the json content that the release dates for the results should be within range of the dates listed
	 * at the bottom of the json
	 * **Note: Actually found a bug where the first item in the list is not within the date range parameters
	 * listed in the json.
	 * 
	 * Test steps for 401 code:
	 * 1. Setup get api URL with invalid parameters for api_key
	 * 2. Execute the call
	 * 3. Check return response for the following:
	 * a. Status code of 401 (forbidden)
	 * b. content type should be json
	 * c. status message should read invalid api key
	 * 
	 * Test steps for 422 code:
	 * 1. Setup get api URL with invalid parameter for page number > 1000
	 * 2. Execute the call
	 * 3. Check return response for the following:
	 * a. Status code of 422
	 * b. content type should be json
	 * c. check error content information
	 * 
	 * Check different data when using different page numbers
	 * 1. Setup get api URL with valid parameters with page number 1
	 * 2. Execute the call
	 * 3. Store the json response as a string
	 * a. Verify status code is 200
	 * 4. Setup get api URL with valid parameters with page number 2
	 * 5. Execute the call
	 * 6. Store the json response as a string
	 * 7. Compare json responses from step 3 and 6 and verify they are not equal to each other.
	 * 
	 * 
	 * 
	 */
}
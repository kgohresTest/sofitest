package com.test.constants;

//a class to define variables that will be used in all the runnable tests

public class Constants {
	
	public static final String API_KEY = System.getProperty("apiKey"); //grabbed from the VM args
	
	public static final String DEFAULT_URL = "https://api.themoviedb.org/3";
	
	public static final String API_PARAM = System.getProperty("apiParam"); //grabbed from the VM args
	
	public static final String API_PARAM2 = System.getProperty("apiParam2");//grabbed from the VM args
	
	public static final int ID = Integer.parseInt(System.getProperty("idValue"));//grabbed from the VM args

	
	public String urlBuilder(String paramAdditions, String apiParam)
	{
		String newUrl = null;
		
		if (paramAdditions.equals(null))
			System.out.println("Must have additions to the parameter, i.e for example {id}?api_key={API_KEY}");
		else
			newUrl = DEFAULT_URL+apiParam+paramAdditions;
		
		return newUrl;
	}
}